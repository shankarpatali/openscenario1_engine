################################################################################
# Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

import pytest

from main import Model, get_from_json

@pytest.fixture
def umlModel():
    return get_from_json()

def test_get_from_json(umlModel):
    assert len(umlModel) > 0

def test_model_transient_values(umlModel):
    model = Model(umlModel, [('','', '')])
    assert len(model.conditions['RelativeLanePosition'].values) > 0

def test_model_without_annotation(umlModel):
    model = Model(umlModel, [('','', '')])
    assert model.elements['RelativeLanePosition'].is_leaf == False

def test_model_with_annotation(umlModel):
    model = Model(umlModel, [
        ('Orientation', 'some_type', '"some_header.h"'),
        ('Entity', 'some_type', '"some_header.h"')
        ,])
    assert model.elements['RelativeLanePosition'].is_leaf == True

def test_model_filter_from_anchor(umlModel):
    model = Model(umlModel, [
        ('Orientation', 'some_type', '"some_header.h"'),
        ('Entity', 'some_type', '"some_header.h"')
        ,])

    assert len(model.get_graph('ByEntityCondition', 'RelativeLanePosition', trim_to_anchor=True)) == 6

def test_model_filter_from_root(umlModel):
    model = Model(umlModel, [
        ('Orientation', 'some_type', '"some_header.h"'),
        ('Entity', 'some_type', '"some_header.h"')
        ,])

    assert len(model.get_graph('ByEntityCondition', 'RelativeLanePosition', trim_to_anchor=False)) == 18

def test_init_action_parser(umlModel):
    model = Model(umlModel, [
        ('Orientation', 'some_type', '"some_header.h"'),
        ('Entity', 'some_type', '"some_header.h"')], dict())
    
    assert len(model.children('InitActions')) == 1