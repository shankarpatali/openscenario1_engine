/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Storyboard/Actions/PrivateAction.h"

#include <vector>

namespace OPENSCENARIO
{

class MotionControlAction : public PrivateAction
{
  public:
    MotionControlAction(std::shared_ptr<mantle_api::IEnvironment> environment, const std::vector<std::string>& actors);

    void Step() final;

  private:
    void ExecuteStartTransition() final;
    virtual void StartAction() = 0;
    virtual bool HasControlStrategyGoalBeenReached(const std::string& actor) = 0;
    virtual bool IsLongitudinal() = 0;
    virtual bool IsLateral() = 0;
    void ResetLongitudinalControlStrategyToDefault(const std::string& actor);
    void ResetLateralControlStrategyToDefault(const std::string& actor);

    std::vector<std::string> actors_with_running_control_strategy_;
};

}  // namespace OPENSCENARIO
