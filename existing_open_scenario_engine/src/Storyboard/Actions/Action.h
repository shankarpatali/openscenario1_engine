/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Storyboard/StoryboardElement.h"

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

namespace OPENSCENARIO
{

class Action : public StoryboardElement
{
  public:
    explicit Action(std::shared_ptr<mantle_api::IEnvironment> environment);

  protected:
    std::shared_ptr<mantle_api::IEnvironment> environment_;
};

}  // namespace OPENSCENARIO
