/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSignalStateAction.h"

namespace OPENSCENARIO
{

TrafficSignalStateAction::TrafficSignalStateAction(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalStateAction> traffic_signal_state_action_data,
    std::shared_ptr<mantle_api::IEnvironment> environment)
    : GlobalAction(environment), traffic_signal_state_action_data_(traffic_signal_state_action_data)
{
}

void TrafficSignalStateAction::ExecuteStartTransition()
{
    std::string name = traffic_signal_state_action_data_->GetName();
    std::string state = traffic_signal_state_action_data_->GetState();

    environment_->SetTrafficSignalState(name, state);
}

void TrafficSignalStateAction::Step()
{
    End();
}

}  // namespace OPENSCENARIO
