/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG),
 *               2021 in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SimulationTimeCondition.h"

#include <MantleAPI/Common/time_utils.h>

namespace OPENSCENARIO
{

bool SimulationTimeCondition::IsSatisfied() const
{
    return rule_.IsSatisfied(environment_->GetSimulationTime().convert<units::time::second>()());
}

}  // namespace OPENSCENARIO
