/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG),
 *               2021 in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Condition.h"

namespace OPENSCENARIO
{

template <typename... Params>
ByAnyCondition toCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICondition> condition, Params&&... args)
{
    if (auto by_entity_condition = condition->GetByEntityCondition(); by_entity_condition)
    {
        auto entity_condition = by_entity_condition->GetEntityCondition();
        auto triggering_entities = by_entity_condition->GetTriggeringEntities();
        if (!entity_condition || !triggering_entities)
        {
            throw std::runtime_error("Cannot specialize ICondition");
        }
        return toCondition(entity_condition, triggering_entities, std::forward<Params>(args)...);
    }
    if (auto by_value_condition = condition->GetByValueCondition(); by_value_condition)
    {
        return toCondition(by_value_condition, std::forward<Params>(args)...);
    }
    throw std::runtime_error("Cannot specialize ICondition");
}

Condition::Condition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICondition> condition,
                     std::shared_ptr<mantle_api::IEnvironment> environment)
    : env_{environment}, delay_(mantle_api::SecondsToTime(condition->GetDelay()))
{
    const auto conditionEdge = condition->GetConditionEdge();
    evaluator_ = toEdgeEvaluator(conditionEdge);
    condition_ = toCondition(condition, environment);
}

bool Condition::IsSatisfied()
{
    static bool first_time = true;
    if (first_time)
    {
        start_time_ = env_->GetSimulationTime();
        first_time = false;
    }

    auto is_satisfied = std::visit(Condition::EvaluateByAnyCondition{}, condition_);
    evaluator_->UpdateStatus(is_satisfied);
    results_[env_->GetSimulationTime()] = evaluator_->GetStatus();

    auto previous_result_time = env_->GetSimulationTime() - delay_;
    if (previous_result_time < start_time_)  // Earlier than first recorded result
    {
        return false;
    }

    auto it = results_.find(previous_result_time);
    // If there is no result for the exact previous time, then take the result of one step before
    if (it == results_.end())
    {
        it = results_.lower_bound(previous_result_time);
        it--;
    }
    // Erase all results from before to avoid memory leak
    results_.erase(results_.begin(), it);
    return results_.begin()->second;
}

}  // namespace OPENSCENARIO
