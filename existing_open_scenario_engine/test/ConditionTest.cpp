/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Conditions/Condition.h"
#include "TestUtils.h"

#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_1/impl/ApiClassImplV1_1.h>

namespace OPENSCENARIO
{
using units::literals::operator""_ms;

class ConditionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override
    {
        OpenScenarioEngineLibraryTestBase::SetUp();
        EXPECT_CALL(*env_, GetSimulationTime()).WillRepeatedly([this]() { return step_count_ * 10_ms; });
    }

    std::uint16_t step_count_{0};
};

TEST_F(ConditionTestFixture, GivenFulfilledSimulationTimeConditionWithoutDelay_WhenCallingIsSatisfied_ThenReturnTrue)
{
    auto scenario_simulation_time_condition =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::SimulationTimeConditionImpl>();
    scenario_simulation_time_condition->SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_OR_EQUAL);
    scenario_simulation_time_condition->SetValue(0.0);

    auto scenario_by_value_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::ByValueConditionImpl>();
    scenario_by_value_condition->SetSimulationTimeCondition(scenario_simulation_time_condition);

    auto scenario_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::ConditionImpl>();
    scenario_condition->SetConditionEdge(NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::NONE);
    scenario_condition->SetDelay(0.0);
    scenario_condition->SetByValueCondition(scenario_by_value_condition);

    Condition condition(scenario_condition, env_);

    EXPECT_TRUE(condition.IsSatisfied());
}

TEST_F(ConditionTestFixture,
       GivenFulfilledSimulationTimeConditionWithDelay_WhenCallingIsSatisfied_ThenReturnTrueAfterDelay)
{
    auto scenario_simulation_time_condition =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::SimulationTimeConditionImpl>();
    scenario_simulation_time_condition->SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_OR_EQUAL);
    scenario_simulation_time_condition->SetValue(0.0);

    auto scenario_by_value_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::ByValueConditionImpl>();
    scenario_by_value_condition->SetSimulationTimeCondition(scenario_simulation_time_condition);

    auto scenario_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::ConditionImpl>();
    scenario_condition->SetConditionEdge(NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::NONE);
    scenario_condition->SetDelay(0.02);
    scenario_condition->SetByValueCondition(scenario_by_value_condition);

    Condition condition(scenario_condition, env_);

    EXPECT_FALSE(condition.IsSatisfied());
    step_count_++;
    EXPECT_FALSE(condition.IsSatisfied());
    step_count_++;
    EXPECT_TRUE(condition.IsSatisfied());
    step_count_++;
    EXPECT_TRUE(condition.IsSatisfied());
}

TEST_F(
    ConditionTestFixture,
    GivenFulfilledSimulationTimeConditionWithDelayNotDivisableByStepTime_WhenCallingIsSatisfied_ThenReturnTrueAfterDelay)
{
    auto scenario_simulation_time_condition =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::SimulationTimeConditionImpl>();
    scenario_simulation_time_condition->SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_OR_EQUAL);
    scenario_simulation_time_condition->SetValue(0.0);

    auto scenario_by_value_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::ByValueConditionImpl>();
    scenario_by_value_condition->SetSimulationTimeCondition(scenario_simulation_time_condition);

    auto scenario_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::ConditionImpl>();
    scenario_condition->SetConditionEdge(NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::NONE);
    scenario_condition->SetDelay(0.0123);
    scenario_condition->SetByValueCondition(scenario_by_value_condition);

    Condition condition(scenario_condition, env_);

    EXPECT_FALSE(condition.IsSatisfied());
    step_count_++;
    EXPECT_FALSE(condition.IsSatisfied());
    step_count_++;
    EXPECT_TRUE(condition.IsSatisfied());
    step_count_++;
    EXPECT_TRUE(condition.IsSatisfied());
}

}  // namespace OPENSCENARIO
