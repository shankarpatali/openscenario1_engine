/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Actions/VisibilityAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using namespace units::literals;

class VisibilityActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
};

MATCHER_P(VisConfigEqualTo, expected_vis_config, "")
{
    return arg.graphics == expected_vis_config.graphics && arg.traffic == expected_vis_config.traffic &&
           arg.sensors == expected_vis_config.sensors;
}

TEST_F(VisibilityActionTestFixture, GivenVisibilityActionWithOneActor_WhenStepAction_ThenSetVisibilityCallOnce)
{
    using namespace OPENSCENARIO::TESTING;
    auto fake_visibility_action = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::VisibilityActionImpl>();
    OPENSCENARIO::VisibilityAction visibility_action{
        fake_visibility_action, env_, std::vector<std::string>{"TrafficVehicle"}};

    bool expected_graphic_value = false;
    bool expected_sensors_value = true;
    bool expected_traffic_value = true;

    fake_visibility_action->SetTraffic(expected_traffic_value);
    fake_visibility_action->SetSensors(expected_sensors_value);
    fake_visibility_action->SetGraphics(expected_graphic_value);

    const mantle_api::EntityVisibilityConfig expected_vis_config{
        expected_graphic_value, expected_traffic_value, expected_sensors_value, {"test_sensor_name"}};

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("")->get()),
                SetVisibility(VisConfigEqualTo(expected_vis_config)))
        .Times(1);

    visibility_action.Start();
    EXPECT_NO_THROW(visibility_action.Step());
}

TEST_F(VisibilityActionTestFixture, GivenVisibilityActionWithTwoActor_WhenStepAction_ThenSetVisibilityCallTwice)
{
    using namespace OPENSCENARIO::TESTING;
    auto fake_visibility_action = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::VisibilityActionImpl>();
    OPENSCENARIO::VisibilityAction visibility_action{
        fake_visibility_action, env_, std::vector<std::string>{"TrafficVehicle1", "TrafficVehicle2"}};

    bool expected_default_value = false;

    const mantle_api::EntityVisibilityConfig expected_vis_config{
        expected_default_value, expected_default_value, expected_default_value, {"test_sensor_name"}};

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("")->get()),
                SetVisibility(VisConfigEqualTo(expected_vis_config)))
        .Times(2);

    visibility_action.Start();
    EXPECT_NO_THROW(visibility_action.Step());
}

}  // namespace OPENSCENARIO
