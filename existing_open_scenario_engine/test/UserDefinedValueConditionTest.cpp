/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Conditions/ByValueConditions/UserDefinedValueCondition.h"
#include "TestUtils.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{

class UserDefinedValueConditionTestFixture : public OpenScenarioEngineLibraryTestBase
{
};

TEST_F(UserDefinedValueConditionTestFixture,
       GivenUserDefinedValueCondition_WhenValueIsNotSet_ThenIsSatisfiedReturnsFalse)
{
    auto scenario_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::UserDefinedValueConditionImpl>();
    scenario_condition->SetName("Trigger1");
    scenario_condition->SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO);
    scenario_condition->SetValue("true");
    OPENSCENARIO::UserDefinedValueCondition sut_condition{scenario_condition, env_};

    EXPECT_FALSE(sut_condition.IsSatisfied());
}

TEST_F(UserDefinedValueConditionTestFixture,
       GivenUserDefinedValueCondition_WhenValueIsDifferent_ThenIsSatisfiedReturnsFalse)
{
    auto scenario_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::UserDefinedValueConditionImpl>();
    scenario_condition->SetName("Trigger1");
    scenario_condition->SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO);
    scenario_condition->SetValue("true");
    OPENSCENARIO::UserDefinedValueCondition sut_condition{scenario_condition, env_};
    ON_CALL(*env_, GetUserDefinedValue(testing::_)).WillByDefault(testing::Return("false"));

    EXPECT_FALSE(sut_condition.IsSatisfied());
}

TEST_F(UserDefinedValueConditionTestFixture, GivenUserDefinedValueCondition_WhenValueIsSame_ThenIsSatisfiedReturnsTrue)
{
    auto scenario_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::UserDefinedValueConditionImpl>();
    scenario_condition->SetName("Trigger1");
    scenario_condition->SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO);
    scenario_condition->SetValue("true");
    OPENSCENARIO::UserDefinedValueCondition sut_condition{scenario_condition, env_};
    ON_CALL(*env_, GetUserDefinedValue(testing::_)).WillByDefault(testing::Return("true"));

    EXPECT_TRUE(sut_condition.IsSatisfied());
}

}  // namespace OPENSCENARIO
