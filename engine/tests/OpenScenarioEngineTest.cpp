/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OpenScenarioEngine/OpenScenarioEngine.h"

#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <filesystem>

using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_rad;

inline std::string GetScenariosPath(const std::string& test_file_path)
{
    std::filesystem::path file_path{test_file_path};
    return {file_path.remove_filename().string() + "data/Scenarios/"};
}

class OpenScenarioEngineTest : public ::testing::Test
{
  protected:
    void SetUp() override
    {
        ON_CALL(env_->GetControllerRepository(), Create(testing::_)).WillByDefault(testing::ReturnRef(controller_));
        auto& mockEntityRepository = static_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository());

        mantle_api::VehicleProperties ego_properties{};
        ego_properties.type = mantle_api::EntityType::kVehicle;
        ego_properties.classification = mantle_api::VehicleClass::kMedium_car;
        ego_properties.model = "medium_car";
        ego_properties.bounding_box.dimension.length = 5.0_m;
        ego_properties.bounding_box.dimension.width = 2.0_m;
        ego_properties.bounding_box.dimension.height = 1.8_m;
        ego_properties.bounding_box.geometric_center.x = 1.4_m;
        ego_properties.bounding_box.geometric_center.y = 0.0_m;
        ego_properties.bounding_box.geometric_center.z = 0.9_m;
        ego_properties.performance.max_acceleration = 10_mps_sq;
        ego_properties.performance.max_deceleration = 10_mps_sq;
        ego_properties.performance.max_speed = 70_mps;
        ego_properties.front_axle.bb_center_to_axle_center = {1.58_m, 0.0_m, -0.5_m};
        ego_properties.front_axle.max_steering = 0.5_rad;
        ego_properties.front_axle.track_width = 1.68_m;
        ego_properties.front_axle.wheel_diameter = 0.8_m;
        ego_properties.rear_axle.bb_center_to_axle_center = {-1.4_m, 0.0_m, -0.5_m};
        ego_properties.rear_axle.max_steering = 0_rad;
        ego_properties.rear_axle.track_width = 1.68_m;
        ego_properties.rear_axle.wheel_diameter = 0.8_m;
        ego_properties.is_host = true;
        // Necessary because Create() is always called in engine init and will otherwise not return a MockVehicle ref
        // which results in an exception
        ON_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(testing::_, ego_properties))
            .WillByDefault(testing::ReturnRef(mock_vehicle_));
    }

    std::shared_ptr<mantle_api::MockEnvironment> env_{std::make_shared<mantle_api::MockEnvironment>()};
    std::string default_xosc_scenario_{"OSC_1_1_test.xosc"};
    mantle_api::MockVehicle mock_vehicle_{};
    mantle_api::MockController controller_;
};

TEST_F(OpenScenarioEngineTest, GivenScenarioWithOneSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsDuration)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};

    EXPECT_CALL(*env_, SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute)).Times(1);

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().scenario_timeout_duration, units::time::second_t(300.0));
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithTwoSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsMaxDuration)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test_TwoSimulationTimeConditions.xosc"};

    EXPECT_CALL(*env_, SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute)).Times(1);

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().scenario_timeout_duration, units::time::second_t(300.0));
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithNoSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsMaxDouble)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test_NoSimulationTimeCondition.xosc"};

    EXPECT_CALL(*env_, SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute)).Times(1);

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().scenario_timeout_duration, mantle_api::Time{std::numeric_limits<double>::max()});
}
