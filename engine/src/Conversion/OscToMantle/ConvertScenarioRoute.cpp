/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioRoute.h"

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Traffic/control_strategy.h>

#include <iostream>

namespace OPENSCENARIO
{
namespace detail
{
std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> GetRoute(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> route)
{
  return route;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> ConvertCatalogReferenceToRoute(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalogReference)
{
  auto catalogElement = catalogReference->GetRef();
  return NET_ASAM_OPENSCENARIO::v1_1::CatalogHelper::AsRoute(catalogElement);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> ResolveChoice(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> route,
                                                                   std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalogReference)
{
  if (route) return GetRoute(route);
  if (catalogReference) return ConvertCatalogReferenceToRoute(catalogReference);
  throw std::runtime_error("ConvertScenarioRoute: No route defined or referenced. Please adjust the scenario.");
}

mantle_api::RouteStrategy ConvertRouteStrategy(
    const NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum& route_strategy_enum)
{
  switch (route_strategy_enum)
  {
    case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::FASTEST:
      return mantle_api::RouteStrategy::kFastest;
    case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::LEAST_INTERSECTIONS:
      return mantle_api::RouteStrategy::kLeastIntersections;
    case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::SHORTEST:
      return mantle_api::RouteStrategy::kShortest;
    case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::RANDOM:
      [[fallthrough]];
    case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::UNKNOWN:
      [[fallthrough]];
    default:
      return mantle_api::RouteStrategy::kUndefined;
  }
}

std::vector<mantle_api::RouteWaypoint> ConvertWaypoints(std::shared_ptr<mantle_api::IEnvironment> environment, std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> route_object)
{
  const auto route_waypoints = route_object->GetWaypoints();
  std::vector<mantle_api::RouteWaypoint> waypoints;

  for (size_t i = 0; i < route_waypoints.size(); ++i)
  {
    const auto& waypoint = route_waypoints[i];

    if (auto pose = ConvertScenarioPosition(environment, waypoint->GetPosition()))
    {
      waypoints.emplace_back(
          mantle_api::RouteWaypoint{
              pose->position,
              ConvertRouteStrategy(NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::GetFromLiteral(waypoint->GetRouteStrategy().GetLiteral()))});
    }
    else
    {
      std::cout << "ConvertScenarioRoute: Could not convert scenario position to global position for route "
                << route_object->GetName() << ", waypoint index " << i << ". Omitting waypoint" << std::endl;
    }
  }
  return waypoints;
}

}  //namespace detail

Route ConvertScenarioRoute(std::shared_ptr<mantle_api::IEnvironment> environment,
                           std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> route_object,
                           std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalogReference)
{
  route_object = detail::ResolveChoice(route_object, catalogReference);
  return {route_object->GetClosed(), detail::ConvertWaypoints(environment, route_object)};
}

}  // namespace OPENSCENARIO