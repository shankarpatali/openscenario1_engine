#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>
#include <string>

namespace OPENSCENARIO
{
struct TimeHeadwayCondition
{
  //TODO: Need to implement the correct types
  bool alongRoute;
  CoordinateSystem coordinateSystem;
  Entity entity;
  bool freeSpace;
  RelativeDistanceType relativeDistanceType;
  Rule rule;
  double value;
};

inline TimeHeadwayCondition ConvertScenarioTimeHeadwayCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITimeHeadwayCondition> timeHeadwayCondition)
{
  return {
      timeHeadwayCondition->GetAlongRoute(),
      ConvertScenarioEntity(relativeSpeedCondition->GetEntityRef()),
      timeHeadwayCondition->GetFreespace(),
      timeHeadwayCondition->GetRule(),
      -- > timeHeadwayCondition->GetValue()

  };
}

}  // namespace OPENSCENARIO