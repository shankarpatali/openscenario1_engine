/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/LaneOffsetAction_impl.h"

#include <MantleAPI/Traffic/control_strategy.h>

#include <iostream>

namespace OpenScenarioEngine::v1_1
{
void LaneOffsetAction::SetControlStrategy()
{
  // Note:
  // - Access to values parse to mantle/ose datatypes: this->values.xxx
  // - Access to mantle interfaces: this->mantle.xxx
  std::cout << "Method LaneOffsetAction::SetControlStrategy() not implemented yet\n";
}

bool LaneOffsetAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
  // Note:
  // - Access to values parse to mantle/ose datatypes: this->values.xxx
  // - Access to mantle interfaces: this->mantle.xxx
  std::cout << "Method LaneOffsetAction::HasControlStrategyGoalBeenReached() not implemented yet (returning \"true\" by default)\n";
  return true;
}

mantle_api::MovementDomain LaneOffsetAction::GetMovementDomain() const
{
  return mantle_api::MovementDomain::kLateral;
}

}  // namespace OpenScenarioEngine::v1_1